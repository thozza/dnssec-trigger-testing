# -*- coding: utf-8 -*-
#
# Test suite for dnssec-trigger-script written in behave
# Copyright (C) 2015  Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# he Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Author: Tomas Hozza <thozza@redhat.com>

Feature: dnssec-trigger-script behavior testing
  The dnssec-trigger-script is a vital part of client side
  DNSSEC validation. It communicates with NetworkManager and
  reconfigures dnssec-trigger daemon and Unbound based on the
  current network configuration. We need to test the behavior
  of the script based on various network configurations.

  Scenario: one connection, No DNSSEC, No VPN
    Given NetworkManager "is" running
    And NetworkManager "is" handling resolv.conf
    And active connections are configured
      | device   | ip4_default | ip4_domains | ip4_nameservers | ip6_default | ip6_domains | ip6_nameservers | vpn |
      | ethernet | Yes         | example.com | 1.2.3.4         | No          |             |                 | No  |
    And unbound is configured with forward zones
      | domain      | validated | resolvers |
      | example.com | No        | 4.3.2.1   |
    When dnsssec-trigger-script is run with "--prepare"
    Then "/etc/resolv.conf" contains "nameserver 127.0.0.1"