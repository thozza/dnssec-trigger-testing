# -*- coding: utf-8 -*-
#
# Test suite for dnssec-trigger-script written in behave
# Copyright (C) 2015  Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# he Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Author: Tomas Hozza <thozza@redhat.com>

from steps.utils.fake_fs_root import FakeFsRoot


def before_scenario(context, scenario):
    """
    Run before any scenario.

    :param context:
    :param scenario:
    :return:
    """
    context.mock_objects = dict()

    # Setup fake FS root in /tmp
    context.mock_objects[FakeFsRoot.NAME] = FakeFsRoot()
    context.mock_objects[FakeFsRoot.NAME].setup()


def after_scenario(context, scenario):
    """
    Run after each scenario.

    :param context:
    :param scenario:
    :return:
    """
    context.mock_objects[FakeFsRoot.NAME].teardown()


def before_all(context):
    pass