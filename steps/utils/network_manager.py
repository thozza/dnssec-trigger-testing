# -*- coding: utf-8 -*-
#
# Test suite for dnssec-trigger-script written in behave
# Copyright (C) 2015  Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# he Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Author: Tomas Hozza <thozza@redhat.com>

"""
Module containing dummy NetworkManager API objects for mocking purposes
"""

import uuid
import socket
import struct
from binascii import hexlify
from collections import namedtuple


class FakeNMDevice(object):
    """
    dummy class representing NM connection device
    """

    PyNetworkManagerDeviceType = namedtuple('PyNetworkManagerDeviceType', ('value_name', 'value_nick'))

    TYPE_WIFI = 'NM_DEVICE_TYPE_WIFI'
    TYPE_ETHERNET = 'NM_DEVICE_TYPE_ETHERNET'
    TYPE_GENERIC = 'NM_DEVICE_TYPE_GENERIC'

    def __init__(self, dev_type=None):
        if dev_type == self.TYPE_WIFI:
            self._type = self.PyNetworkManagerDeviceType(dev_type, 'wifi')
        elif dev_type == self.TYPE_ETHERNET:
            self._type = self.PyNetworkManagerDeviceType(dev_type, 'ethernet')
        else:
            self._type = self.PyNetworkManagerDeviceType(self.TYPE_GENERIC, 'generic')

    def get_device_type(self):
        # need to return something that has "value_name" member
        return self._type


class FakeIPConfig(object):
    """
    dummy IPv4 and IPv6 config object from NM
    """

    def __init__(self, domains=None, nameservers=None):
        self._domains = domains if domains is not None else list()
        self._nameservers = nameservers if nameservers is not None else list()

    def get_domains(self):
        return self._domains

    # IP4Config interface
    def get_nameservers(self):
        return self._nameservers

    # IP6Config interface
    def get_nameserver(self, index):
        return self._nameservers[index]

    def get_num_nameservers(self):
        return len(self._nameservers)

    @staticmethod
    def ip4_from_str(ip4):
        """Converts IPv4 address from string to integer."""
        return struct.unpack("!L", socket.inet_aton(ip4))[0]

    @staticmethod
    def ip6_from_str(ip6):
        """Converts IPv6 address from string to integer."""
        return int(hexlify(socket.inet_pton(socket.AF_INET6, ip6)), 16)


class FakeNMConnection(object):
    """
    dummy class representing NM connection object
    """

    def __init__(self, default=False, default6=False, devices=None, ip4_conf=None, ip6_conf=None, vpn=False):
        self._default = default
        self._default6 = default6
        self._devices = devices if devices is not None else list()
        self._ip4_conf = ip4_conf
        self._ip6_conf = ip6_conf
        # generate a random UUID
        self._uuid = str(uuid.uuid4())
        self._vpn = vpn

    def get_default(self):
        return self._default

    def get_default6(self):
        return self._default6

    def get_uuid(self):
        return self._uuid

    def get_devices(self):
        # Must return list!
        return self._devices

    def get_ip4_config(self):
        return self._ip4_conf

    def get_ip6_config(self):
        return self._ip6_conf

    def get_vpn(self):
        return self._vpn


class FakeNMClient(object):
    """
    Class to mimic NMClient
    """

    NAME = 'NMClient'

    def __init__(self, nm_running=True, active_connections=None):
        self._manager_running = nm_running
        self._active_connections = active_connections if active_connections is not None else list()

    def new(self):
        """
        This is for implementing NMClient.Client().new()
        """
        return self.Client()

    def Client(self):
        return self

    def get_manager_running(self):
        """
        If NM is running

        :return: True if NM is running, False otherwise
        """
        return self._manager_running

    def get_active_connections(self):
        """
        Get current Active connections

        :return: list of connections OR None
        """
        # TODO: investigate if NM returns empty list or None
        return self._active_connections