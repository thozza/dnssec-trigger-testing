# -*- coding: utf-8 -*-
#
# Test suite for dnssec-trigger-script written in behave
# Copyright (C) 2015  Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# he Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Author: Tomas Hozza <thozza@redhat.com>

"""
Mocking object and methods for simulating fake file system root
"""

import imp
import shutil
import tempfile

import copy

# This is to make sure we call the original unpatched modules
original_os = imp.load_module('original_os', *imp.find_module('os'))
original_open = copy.deepcopy(open)


class FakeFsRoot(object):
    """
    Will setup temporary environment in /tmp
    """

    NAME = 'FakeFsRoot'

    FAKE_ROOT_PATH = ''
    ORIGINAL_PATH = original_os.getcwd()

    FAKE_DIR_STRUCTURE = [
        '/etc',
        '/etc/NetworkManager',
        '/usr',
        '/usr/bin',
        '/usr/sbin',
        '/usr/lib',
        '/usr/lib64',
        '/var',
        '/run',
        '/run/dnssec-triggerd',
    ]

    FAKE_DIR_STRUCTURE_SYMLINKS = [
        ('/run', '/var/run'),
        ('/usr/bin', '/bin'),
        ('/usr/sbin', '/sbin'),
        ('/usr/lib', '/lib'),
        ('/usr/lib64', '/lib64'),
    ]

    def setup(self):
        """
        Setup the temporary environment and change the working directory to it.
        """
        self.FAKE_ROOT_PATH = tempfile.mkdtemp(prefix="fake-fs-root-")
        self._setup_dir_structure()
        original_os.chdir(self.FAKE_ROOT_PATH)

    def teardown(self):
        """
        Destroy the temporary environment.
        :return:
        """
        original_os.chdir(self.ORIGINAL_PATH)
        shutil.rmtree(self.FAKE_ROOT_PATH)

    def _setup_dir_structure(self):
        """
        Create needed directories in the fake root dir
        :return:
        """
        for d in self.FAKE_DIR_STRUCTURE:
            self.os_makedirs(d)

        for src, l_name in self.FAKE_DIR_STRUCTURE_SYMLINKS:
            self.os_symlink(src, l_name)

    def path(self):
        return self.FAKE_ROOT_PATH

    def _get_fake_path(self, path):
        return original_os.path.join(self.FAKE_ROOT_PATH, original_os.path.abspath(path).lstrip(original_os.sep))

    def builtin_open(self, path, *args, **kwargs):
        return original_open(self._get_fake_path(path), *args, **kwargs)

    def os_rename(self, src, dst):
        return original_os.rename(self._get_fake_path(src), self._get_fake_path(dst))

    def os_symlink(self, source, link_name):
        return original_os.symlink(self._get_fake_path(source), self._get_fake_path(link_name))

    def os_remove(self, path):
        return original_os.remove(self._get_fake_path(path))

    def os_open(self, file, *args, **kwargs):
        return original_os.open(self._get_fake_path(file), *args, **kwargs)

    def os_makedirs(self, path, *args, **kwargs):
        return original_os.makedirs(self._get_fake_path(path), *args, **kwargs)

    def os_path_exists(self, path):
        return original_os.path.exists(self._get_fake_path(path))
