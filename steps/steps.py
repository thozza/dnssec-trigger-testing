# -*- coding: utf-8 -*-
#
# Test suite for dnssec-trigger-script written in behave
# Copyright (C) 2015  Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# he Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Author: Tomas Hozza <thozza@redhat.com>

try:
    from unittest import mock
except ImportError:
    import mock

from behave import *

from utils.network_manager import FakeNMClient, FakeNMConnection, FakeNMDevice, FakeIPConfig
from utils.fake_fs_root import FakeFsRoot


@given('NetworkManager "{state}" running')
def step_impl(context, state):
    """
    Create Fake NMClient object
    :param context:
    :param state: if 'is', then NM is setup as running, otherwise as not running
    :return: None
    """
    if state == 'is':
        context.mock_objects[FakeNMClient.NAME] = FakeNMClient(nm_running=True)
    else:
        context.mock_objects[FakeNMClient.NAME] = FakeNMClient(nm_running=False)


@given('active connections are configured')
def step_impl(context):
    """
    Setup Active Connections. Expects table as the argument of the step, with the following columns:

    device - type of NMDevice (ethernet|wifi|generic)
    ip4_default - If default connection for IPv4 (Yes|No)
    ip4_domains - list of IPv4 search domains
    ip4_nameservers - list of IPv4 nameservers
    ip6_default - if default connection for IPv6 (Yes|No)
    ip6_domains - list of IPv6 search domains
    ip6_nameservers - list of IPv6 nameservers
    vpn - whether the connection is VPN (Yes|No)

    :param context:
    :return:
    """
    nm_client = context.mock_objects.get(FakeNMClient.NAME)

    # should not happen
    if nm_client is None:
        nm_client = context.mock_objects[FakeNMClient.NAME] = FakeNMClient()

    for row in context.table:
        # device
        if row['device'] == 'ethernet':
            device = FakeNMDevice(FakeNMDevice.TYPE_ETHERNET)
        elif row['device'] == 'wifi':
            device = FakeNMDevice(FakeNMDevice.TYPE_WIFI)
        else:
            device = FakeNMDevice(FakeNMDevice.TYPE_GENERIC)

        # IPv4 configuration
        ip4_conf = None
        ip4_domains = row['ip4_domains'].split()
        ip4_nameservers = [FakeIPConfig.ip4_from_str(x) for x in row['ip4_nameservers'].split()]

        if ip4_domains or ip4_nameservers:
            ip4_conf = FakeIPConfig(ip4_domains, ip4_nameservers)

        # IPv6 configuration
        ip6_conf = None
        ip6_domains = row['ip6_domains'].split()
        ip6_nameservers = [FakeIPConfig.ip6_from_str(x) for x in row['ip6_nameservers'].split()]

        if ip6_domains or ip6_nameservers:
            ip6_conf = FakeIPConfig(ip6_domains, ip6_nameservers)

        connection = FakeNMConnection(default=True if row['ip4_default'].lower() == 'yes' else False,
                                      ip4_conf=ip4_conf,
                                      default6=True if row['ip6_default'].lower() == 'yes' else False,
                                      ip6_conf=ip6_conf,
                                      devices=[device],
                                      vpn=True if row['vpn'].lower() == 'yes' else False)

        # add the connection
        nm_client.get_active_connections().append(connection)


@given('NetworkManager "{state}" handling resolv.conf')
def step_impl(context, state):
    """
    Whether configure NM as handling /etc/resolv.conf
    :param context:
    :param state: if 'is', then NM is handling resolv.conf, otherwise is not
    :return: None
    """
    fake_fs = context.mock_objects[FakeFsRoot.NAME]

    with mock.patch('__builtin__.open', fake_fs.builtin_open):
        with open('/etc/NetworkManager/NetworkManager.conf', 'wb') as conf:
            conf.write('[main]\n')
            if state != 'is':
                conf.write('dns=unbound')


@given('unbound is configured with forward zones')
def step_impl(context):
    pass


@when('dnsssec-trigger-script is run with "{arguments}"')
def step_impl(context, arguments):
    pass


@then('"{file}" contains "{string}"')
def step_impl(context, file, string):
    pass

